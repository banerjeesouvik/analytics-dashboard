## Quick Setup
1. Clone the repo
2. `cd analytics-dashboard`
3. `npm i`
4. `npm start`

The app runs on port `3000`, so make sure that no other application is using the smae port