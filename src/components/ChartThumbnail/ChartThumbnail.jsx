import React, { useState, Fragment } from "react";
import ChartForm from "components/ChartForm";
import Popup from "components/Popup";
import ChartistGraph from "react-chartist";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import { Divider, Button, TextField } from "@material-ui/core";
import GeneratedChart from "components/GeneratedChart/GeneratedChart";

function ChartThumbnail({ data, type, dataSources }) {
  const [chartConfigs, setChartConfigs] = useState([]);
  const [showChartPopup, setShowChartPopup] = useState(false);
  const [showGeneratedChart, handleShowGeneratedChart] = useState(false);

  const handleAddChart = () => {
    setShowChartPopup(!showChartPopup);
  };

  const handleSubmitAddChart = chartDetails => {
    setChartConfigs([...chartConfigs, chartDetails]);
  };

  const handleGenerateChart = () => {
    handleAddChart();
    handleShowGeneratedChart(!showGeneratedChart);
  };

  return (
    <div onClick={handleAddChart}>
      <ChartistGraph className="ct-chart" data={data} type={type} />
      {showChartPopup && (
        <Popup childClass="add-relation-popup" onClose={handleAddChart}>
          <GridContainer>
            <GridItem xs={12} sm={6} md={6}>
              <ChartForm
                onSubmit={handleSubmitAddChart}
                dataSources={dataSources}
              />
            </GridItem>
            <Divider orientation="vertical" />
            <GridItem xs={8} sm={6} md={6}>
              <h4>Saved Relations</h4>
              {chartConfigs.map((conf, index) => (
                <TextField
                  key={index}
                  multiline
                  readOnly
                  rows={3}
                  value={Object.keys(conf).map(key => `${key} -> ${conf[key]}`)}
                />
              ))}
              <h4 />
              <Button
                disabled={!chartConfigs.length}
                color="primary"
                onClick={handleGenerateChart}
                variant="contained"
              >
                Generate Chart
              </Button>
            </GridItem>
          </GridContainer>
        </Popup>
      )}
      {showGeneratedChart && (
        <Popup onClose={handleGenerateChart} childClass="generated-chart-popup">
          <GeneratedChart />
        </Popup>
      )}
    </div>
  );
}

export default ChartThumbnail;
