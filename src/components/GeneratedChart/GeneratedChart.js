import React, { Fragment, useState } from "react";
import Table from "components/Table/Table";
import { Divider, TextField, Button } from "@material-ui/core";
import ChartistGraph from "react-chartist";
import Card from "components/Card/Card";
import CardHeader from "components/Card/CardHeader";

const data = {
  labels: [
    "Brand 1, brand-1",
    "Brand 2, brand-2",
    "Brand 3, brand-3",
    "Brand 4, brand-4"
  ],
  series: [[10, 15, 20, 10]]
};

function GeneratedChart() {
  const [values, setValues] = useState({});
  const [showChart, handleShowChart] = useState(false);

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  const handleDisplayChart = () => {
    handleShowChart(!showChart);
  };

  return (
    <Fragment>
      <Table
        tableHeaderColor="warning"
        tableHead={["brands.name", "products.name"]}
        tableData={[
          ["Brand 1", "product 25"],
          ["Brand 2", ""],
          ["Brand 3", ""],
          ["Maggie", "Maggi Masala"]
        ]}
      />
      <Divider />
      <h4 />
      <form>
        <TextField
          label="Group By"
          value={values.groupBy}
          onChange={handleChange("groupBy")}
        />
        <TextField
          label="Aggregator"
          value={values.aggregator}
          onChange={handleChange("aggregator")}
        />
        <TextField
          label="filter"
          value={values.filter}
          onChange={handleChange("filter")}
        />
        <h5 />
        <div>
          <Button
            onClick={handleDisplayChart}
            variant="contained"
            color="primary"
          >
            Display Chart
          </Button>
        </div>
        {showChart && (
          <Fragment>
            <Card chart>
              <CardHeader color="success">
                <ChartistGraph className="ct-chart" data={data} type="Bar" />
              </CardHeader>
            </Card>
            <h4 />
            <Button
              onClick={handleDisplayChart}
              variant="contained"
              color="primary"
            >
              Save
            </Button>
            <Button
              onClick={handleDisplayChart}
              variant="contained"
              color="secondary"
            >
              Share
            </Button>
          </Fragment>
        )}
      </form>
    </Fragment>
  );
}

export default GeneratedChart;
