import React, { useState, useEffect, Fragment } from "react";
import Loader from "components/Loader";
import ColumnList from "components/ColumnList";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";

import { makeStyles } from "@material-ui/core/styles";

import "./style.css";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";

const useStyles = makeStyles(styles);

function TableList({ selectedDBTable, onClick }) {
  const [tableList, setTableList] = useState();
  const [isLoading, setIsLoading] = useState();

  const classes = useStyles();

  useEffect(() => {
    setIsLoading(true);
    // Fetch lsit of Tables in a DB
    // Todo: Replace with GET http://localhost:9170/information-schema/tableInfo?database=catalogue_service
    fetch("https://api.myjson.com/bins/uinl1")
      .then(resp => resp.json())
      .then(resp => {
        setTableList(resp.data);
        setIsLoading(false);
      });
  }, [selectedDBTable.db]);

  return (
    <div className="table-list-block">
      {isLoading || !tableList ? (
        <Loader />
      ) : (
        <Fragment>
          <div className="table-list-column">
            <GridContainer>
              {tableList.map(({ TableName }) => (
                <GridItem
                  xs={10}
                  sm={10}
                  md={6}
                  key={TableName}
                  onClick={() =>
                    onClick({ ...selectedDBTable, table: TableName })
                  }
                  className="table-columns"
                >
                  <Card>
                    <CardHeader color="warning">
                      <h4 className={classes.cardTitleWhite}>{TableName}</h4>
                    </CardHeader>
                    <ColumnList />
                  </Card>
                </GridItem>
              ))}
            </GridContainer>
          </div>
        </Fragment>
      )}
    </div>
  );
}

export default TableList;
