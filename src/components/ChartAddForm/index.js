import React, { useState } from "react";
import { TextField, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "1rem"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  button: {
    margin: theme.spacing(1)
  }
}));

function ChartAddForm({ onSubmit }) {
  const classes = useStyles();
  const [values, setValues] = useState({});

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  const handelSubmit = e => {
    e && e.preventDefault();
    onSubmit && onSubmit(values);
  };

  return (
    <form
      className={classes.container}
      noValidate
      autoComplete="off"
      onSubmit={handelSubmit}
    >
      <TextField
        label="Name"
        className={classes.textField}
        value={values.name}
        onChange={handleChange("name")}
        margin="normal"
      />
      <TextField
        label="Type"
        className={classes.textField}
        value={values.type}
        onChange={handleChange("type")}
        margin="normal"
      />
      <Button
        variant="contained"
        color="primary"
        className={classes.button}
        onClick={handelSubmit}
      >
        Submit
      </Button>
    </form>
  );
}

export default ChartAddForm;
